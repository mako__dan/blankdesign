var gulp = require("gulp");
var sass = require("gulp-sass");
var browserSync = require('browser-sync').create();
const { parallel } = require('gulp');
	
function style() {
    browserSync.reload();
    return (
        gulp
            .src("shared/*.scss")
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(gulp.dest("css/"))
    );
}

function style_watch(){
    return (
        gulp.watch('shared/*.scss', style)
    )  
}

function html_watch(){
    return (
        gulp.watch('**/*.html', browser_reload)
    )  
}

function php_watch(){
    return (
        gulp.watch('**/*.php', browser_reload)
    )  
}

function browser(){
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
}
function browser_reload(){
    browserSync.reload();
    return (
        gulp
            .src("shareds/*.scss")
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(gulp.dest("css/"))
    );
}
// Expose the task by exporting it
// This allows you to run it from the commandline using
// $ gulp style
exports.style = style;
exports.style_watch = style_watch;
exports.browser = browser;
exports.build = parallel(browser,style_watch,html_watch,php_watch);